package demo;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
//Selenium Imports
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
///


public class Automate_post_on_linkedin {
    ChromeDriver driver;
    public Automate_post_on_linkedin()
    {
        System.out.println("Constructor: TestCases");
        WebDriverManager.chromedriver().timeout(30).setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    public void endTest()
    {
        System.out.println("End Test: TestCases");
        driver.close();
        driver.quit();

    }


    public  void testCase01() throws InterruptedException {
        System.out.println("Start Test case: testCase01");

        driver.get(" https://in.linkedin.com/");
        WebElement Email_bar =driver.findElement(By.id("session_key"));
        Email_bar.sendKeys("pakhiradebanjan@gmail.com");
        WebElement password_bar = driver.findElement(By.id("session_password"));
        password_bar.sendKeys("password");

        WebElement MyProfile = driver.findElement(By.id("ember968"));
        MyProfile.click();
        Thread.sleep(2000);

        WebElement profileViewer = driver.findElement(By.xpath("//*[@id='ember1445']/div[3]/ul/li[1]/div/div[2]/div/a/span/span[1]/text()"));
        profileViewer.click();
        WebElement number_of_profile_viewer  = driver.findElement(By.xpath("//*[@id='ember1595']/div/div/ul/li/div/p"));
        System.out.println("the number of profile viewer: "+number_of_profile_viewer.getText());

        WebElement homeButton =driver.findElement(By.xpath("//*[@id='ember1618']/div/div/li-icon//*[local-name()='svg' and @height='24']"));
        homeButton.click();
        Thread.sleep(2000);

        WebElement start_a_post =driver.findElement(By.xpath("//span[text()='Start a post']"));
        start_a_post.click();

        WebElement WriteText= driver.findElement(By.xpath("//*[@id='ember2437']/div/div[2]/div[1]/div/div/div/div/div/div/div[1]/p"));
        WriteText.sendKeys("test post");

        WebElement post_button =driver.findElement(By.id("ember2466"));
        post_button.click();


        WebElement post =driver.findElement(By.xpath("//*[@id='ember2486']/div/div[4]/div/div/span/span"));
        String text = post.getText();

        if(text.equals("test post")){
            System.out.println("posted successfully");
        }else
            System.out.println("post is not matched");

        System.out.println("end Test case: testCase02");
    }


}
